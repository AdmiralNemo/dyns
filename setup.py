from setuptools import find_packages, setup

setup(
    name='DyNS',
    version='0.1',
    description='Dynamic Nameserver Configuration UI',
    author='Dustin C. Hatch',
    author_email='dustin@hatch.name',
    url='https://bitbucket.org/AdmiralNemo/dyns',
    license='GPL-3',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={
        'dyns': [
            'templates/*',
        ],
    },
    install_requires=[
        'Milla',
        'SQLAlchemy',
    ],
)
