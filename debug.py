from werkzeug import serving
import dyns
import os.path


app = dyns.make_app()


try:
    serving.run_simple(
        hostname='::',
        port=8080,
        application=app,
        static_files={
            '/static': os.path.join(os.path.dirname(__file__), 'static'),
        },
    )
except KeyboardInterrupt:
    print('Exiting...')
