# This file is part of DyNS.
#
# DyNS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DyNS.  If not, see <http://www.gnu.org/licenses/>.
from . import controllers
from milla.dispatch import routing

router = routing.Router()

router.add_route('/', controllers.index)
router.add_route('/zones/', controllers.ZoneListController())
router.add_route('/zones/{name}', controllers.ZoneController())
router.add_route('/records/{id}', controllers.RecordController())
