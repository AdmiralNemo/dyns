# This file is part of DyNS.
#
# DyNS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DyNS.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals
from sqlalchemy import schema, types, orm
from sqlalchemy.ext import declarative


Base = declarative.declarative_base()

Session = orm.session.sessionmaker()

NoResultFound = orm.exc.NoResultFound


class Serializable(object):

    def as_dict(self):
        def serialize(obj):
            for col in obj.__table__.columns:
                value = getattr(obj, col.name)
                if isinstance(value, Serializable):
                    value = dict(serialize(value))
                yield (col.name, value)
        return dict(serialize(self))


class Zone(Base, Serializable):

    __tablename__ = 'zones'

    name = schema.Column(types.Unicode(254), primary_key=True)
    ttl = schema.Column(types.Integer, nullable=False, default=3600)
    source = schema.Column(types.Unicode(254), nullable=False)
    contact = schema.Column(types.Unicode(254), nullable=False)
    serial = schema.Column(types.Integer, nullable=False, default=1024)
    refresh = schema.Column(types.Integer, nullable=False, default=900)
    retry = schema.Column(types.Integer, nullable=False, default=600)
    expire = schema.Column(types.Integer, nullable=False, default=86400)
    minimum = schema.Column(types.Integer, nullable=False, default=3600)
    records = orm.relationship('Record', cascade='delete',
                               order_by='Record.host')


class Record(Base, Serializable):

    __tablename__ = 'records'

    id = schema.Column(types.Integer, autoincrement=True, primary_key=True)
    zone = schema.Column(types.Unicode(254), schema.ForeignKey(Zone.name),
                         nullable=False)
    host = schema.Column(types.Unicode(254), nullable=False)
    ttl = schema.Column(types.Integer, nullable=False, default=3600)
    rdtype = schema.Column(types.Unicode(9), nullable=False)
    mx_prio = schema.Column(types.Integer)
    data = schema.Column(types.UnicodeText)
