# This file is part of DyNS.
#
# DyNS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DyNS.  If not, see <http://www.gnu.org/licenses/>.
from . import model
import jinja2
import json
import milla


class XHTMLResponse(milla.Response):
    default_content_type = 'application/xhtml+xml'


class BaseController(milla.controllers.Controller):

    TMPL_LOADER = jinja2.PackageLoader(__name__.rsplit('.', 1)[0])

    def __before__(self, request):
        super(BaseController, self).__before__(request)
        self.session = model.Session()

        offers = [
            'application/json',
            'application/xhtml+xml',
            'text/html',
        ]
        default_format = request.ResponseClass.default_content_type
        want_fmt = request.accept.best_match(offers, default_format)
        if want_fmt == 'application/json':
            request.want = 'json'
        elif want_fmt == 'text/html':
            request.want = 'html'
        else:
            request.ResponseClass = XHTMLResponse
            request.want = 'xhtml'

        env = jinja2.Environment(loader=self.TMPL_LOADER)
        env.globals.update(
            url=request.create_href,
            static=request.static_resource,
        )
        self.render = lambda t, **k: env.get_template(t).render(**k)

    def __after__(self, request):
        self.session.rollback()
        self.session.bind.dispose()
        del self.session
        del self.render


def index(request):
    raise milla.HTTPMovedPermanently(location=request.create_href('/zones/'))


class ZoneListController(BaseController):

    allowed_methods = ('GET', 'HEAD', 'POST')

    def __call__(self, request):
        return getattr(self, request.method)(request)

    def GET(self, request):
        response = request.ResponseClass()
        zones = self.session.query(model.Zone)
        if request.want == 'json':
            response.content_type = 'application/json'
            json.dump(list(map(model.Zone.as_dict, zones)), response.body_file)
        else:
            if 'new' in request.GET:
                response.text = self.render('new-zone.html.j2')
            else:
                response.text = self.render('index.html.j2', zones=zones)
        return response

    HEAD = GET

    def POST(self, request):
        response = request.ResponseClass()
        if request.content_type == 'application/x-www-form-urlencoded':
            data = dict(request.POST)
        elif request.content_type == 'application/json':
            data = json.loads(request.text)
        else:
            raise milla.HTTPUnsupportedMediaType
        required_fields = ('name', 'contact', 'source')
        for k in list(data.keys()):
            if not data[k]:
                if k not in required_fields:
                    del data[k]
        zone = model.Zone(**data)
        self.session.add(zone)
        self.session.commit()
        location = request.create_href_full('/zones/{}'.format(zone.name))
        if request.want in ('html', 'xhtml'):
            raise milla.HTTPSeeOther(location=location)
        else:
            response.status_int = 201
            response.location = location
        return response


class ZoneController(BaseController):

    allowed_methods = ('GET', 'HEAD', 'POST', 'PUT', 'DELETE')

    def __call__(self, request, name):
        return getattr(self, request.method)(request, name)

    def get_zone(self, name):
        zone = self.session.query(model.Zone).get(name)
        if not zone:
            raise milla.HTTPNotFound
        return zone

    def GET(self, request, name):
        response = request.ResponseClass()
        zone = self.get_zone(name)
        if request.want == 'json':
            response.content_type = 'application/json'
            zone_d = zone.as_dict()
            zone_d['records'] = list(map(model.Record.as_dict, zone.records))
            json.dump(zone_d, response.body_file)
        else:
            if 'new' in request.GET:
                tmpl = 'new-record.html.j2'
            elif 'edit' in request.GET:
                tmpl = 'edit-zone.html.j2'
            else:
                tmpl = 'zone.html.j2'
            response.text = self.render(tmpl, zone=zone)
        return response

    HEAD = GET

    def PUT(self, request, name):
        response = request.ResponseClass()
        response.content_type = None
        if request.content_type == 'application/x-www-form-urlencoded':
            data = dict(request.POST)
        elif request.content_type == 'application/json':
            data = json.loads(request.text)
        else:
            raise milla.HTTPUnsupportedMediaType
        zone = self.get_zone(name)
        for k, v in data.items():
            assert k != 'records'
            assert hasattr(zone, k)
            setattr(zone, k, v)
        self.session.commit()
        if request.want in 'xhtml':
            raise milla.HTTPSeeOther(
                location=request.create_href('/zones/{}'.format(zone.name))
            )
        response.status_int = 204
        return response

    def POST(self, request, name):
        response = request.ResponseClass()
        response.content_type = None
        zone = self.get_zone(name)
        if request.content_type == 'application/x-www-form-urlencoded':
            data = dict(request.POST)
        elif request.content_type == 'application/json':
            data = json.loads(request.text)
        else:
            raise milla.HTTPUnsupportedMediaType
        zone_name = data.pop('zone', zone.name)
        assert zone_name == zone.name
        if 'ttl' in data and not data['ttl']:
            del data['ttl']
        if data['rdtype'] == 'MX':
            try:
                data['mx_prio'] = int(data['mx_prio'])
            except (KeyError, TypeError):
                data['mx_prio'] = 10
        else:
            data.pop('mx_prio', None)
        record = model.Record(zone=zone_name, **data)
        self.session.add(record)
        self.session.commit()
        if request.want in ('html', 'xhtml'):
            raise milla.HTTPSeeOther(
                location=request.create_href('/zones/{}'.format(zone_name))
            )
        else:
            response.status_int = 201
            response.location = request.create_href_full(
                '/records/{}'.format(record.id)
            )
        return response

    def DELETE(self, request, name):
        response = request.ResponseClass()
        response.content_type = None
        zone = self.get_zone(name)
        self.session.delete(zone)
        self.session.commit()
        if request.want in ('html', 'xhtml'):
            raise milla.HTTPSeeOther(location=request.create_href('/zones/'))
        response.status_int = 204
        return response


class RecordController(BaseController):

    allowed_methods = ('GET', 'HEAD', 'PUT', 'DELETE')

    def __call__(self, request, id):
        return getattr(self, request.method)(request, id)

    def get_record(self, id):
        record = self.session.query(model.Record).get(id)
        if not record:
            raise milla.HTTPNotFound
        return record

    def GET(self, request, id):
        response = request.ResponseClass()
        record = self.get_record(id)
        if request.want == 'json':
            response.content_type = 'application/json'
            json.dump(record.as_dict(), response.body_file)
        else:
            response.text = self.render('record.html.j2', record=record)
        return response

    HEAD = GET

    def PUT(self, request, id):
        response = request.ResponseClass()
        response.content_type = None
        record = self.get_record(id)
        if request.content_type == 'application/x-www-form-urlencoded':
            data = dict(request.POST)
        elif request.content_type == 'application/json':
            data = json.loads(request.text)
        else:
            raise milla.HTTPUnsupportedMediaType
        if data['rdtype'] != 'MX':
            data.pop('mx_prio', None)
        for k, v in data.items():
            assert k != 'zone'
            assert hasattr(record, k)
            setattr(record, k, v)
        self.session.commit()
        if request.want in ('html', 'xhtml'):
            raise milla.HTTPSeeOther(
                location=request.create_href('/zones/{}'.format(record.zone))
            )
        response.status_int = 201
        return response

    def DELETE(self, request, id):
        response = request.ResponseClass()
        response.content_type = None
        record = self.get_record(id)
        self.session.delete(record)
        self.session.commit()
        if request.want in ('html', 'xhtml'):
            raise milla.HTTPSeeOther(
                location=request.create_href('/zones/{}'.format(record.zone))
            )
        response.status_int = 204
        return response
