import sqlalchemy
import milla
import milla.util


DEFAULT_CONFIG = {
    'sqlalchemy.url': 'postgresql:///named',
}


def make_app(filename=None):
    from . import routes, model

    app = milla.Application(routes.router)
    app.config.update(DEFAULT_CONFIG)
    if filename:
        app.config.update(milla.util.read_config(filename))

    engine = sqlalchemy.engine_from_config(app.config, 'sqlalchemy.')
    model.Session.configure(bind=engine)

    return app
