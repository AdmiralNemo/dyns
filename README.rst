====
DyNS
====

*DyNS* is a **highly** experimental web front-end for a
dynamically-configurable `ISC BIND`_ authoritative DNS server using
`PostgreSQL`_ as a *Dynamically Loadable Zone* (DLZ) backend database.


.. _ISC BIND: http://www.isc.org/software/bind
.. _PostgreSQL: http://www.postgresql.org/
